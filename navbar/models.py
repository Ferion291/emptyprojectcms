from django.db import models

# Create your models here.
class LinkNavbar(models.Model):
    title = models.CharField(max_length=100,unique=True)
    url = models.CharField(max_length=100, null=True,blank=True)
    ranking =  models.PositiveSmallIntegerField(null=True, blank=True)
    enable_for_site = models.BooleanField(default=True)


    class Meta:
        verbose_name = "Link"

    def __str__(self):
        return self.title
