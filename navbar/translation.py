from modeltranslation.translator import translator, TranslationOptions
from navbar.models import LinkNavbar


class LinkTranslationOptions(TranslationOptions):
    fields = ('title', )

translator.register(LinkNavbar, LinkTranslationOptions)