from django.contrib import admin

from root_setting.models import Social, StaticWord

# Register your models here.
admin.site.register(Social)
admin.site.register(StaticWord)