from django.apps import AppConfig


class RootSettingConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'root_setting'
