
from math import prod
from django.shortcuts import render
from dynamic_page_app.models import *
from django.shortcuts import get_object_or_404, redirect, render
from solidcms__proj.view import BaseView

# Create your views here.
class ClientPageClassView(BaseView):
    data = dict()
    template = "client.html"

    def get(self, request):
        self.data = dict()
        #Must have 
        self.static_translates()
        self.navbar()
        self.footer_contacts()
        self.footer_socials()
        
        #Multilang changer
        generate_url = str(request.get_full_path().split('/')[1])+ "/"
        self.data['page_url'] = str(request.get_full_path()).replace(str(generate_url), '')

        
        #Page specefic data
        self.data['client_list'] = ClientInfo.objects.all()
        return render(request, self.template, self.data)

class CategoryPageClassView(BaseView):
    data = dict()
    template = "categories.html"

    def get(self, request):
        self.data = dict()
        #Must have 
        self.static_translates()
        self.navbar()
        self.footer_contacts()
        self.footer_socials()

        #Multilang changer
        generate_url = str(request.get_full_path().split('/')[1])+ "/"
        self.data['page_url'] = str(request.get_full_path()).replace(str(generate_url), '')

        
        #Page specefic data
        self.data['categories'] = CategoryItem.objects.all().order_by('order')
        return render(request, self.template, self.data)


class CategoryPageByIDClassView(BaseView):
    data = dict()
    template = "products.html"

    def get(self, request):
        self.data = dict()
        #Must have 
        self.static_translates()
        self.navbar()
        self.footer_contacts()
        self.footer_socials()
        
        #Multilang changer
        generate_url = str(request.get_full_path().split('/')[1])+ "/"
        self.data['page_url'] = str(request.get_full_path()).replace(str(generate_url), '')

        #Page specefic data

        self.data['categories'] = CategoryItem.objects.all().order_by('order')

        products  = ProductItem.objects.all().order_by('order')

        category_id = request.GET.get('category' , None)
        code = request.GET.get('code' , None)
        if category_id and category_id.isdigit():
            products = products.filter(category__pk=int(category_id))
            self.data['category_id'] = int(category_id)

        if code:
            products = products.filter(code = code)


        

        self.data['products_for_category'] = products.order_by('order')

        self.data['code']  = code

        
        return render(request, self.template, self.data)


class ProductByIDClassView(BaseView):
    data = dict()
    template = "product_details.html"

    def get(self, request, product_id):
        self.data = dict()
        #Must have 
        self.static_translates()
        self.navbar()
        self.footer_contacts()
        self.footer_socials()
        
        #Multilang changer
        generate_url = str(request.get_full_path().split('/')[1])+ "/"
        self.data['page_url'] = str(request.get_full_path()).replace(str(generate_url), '')

        #Page specefic data


        self.data['product'] = get_object_or_404(ProductItem.objects.prefetch_related('feature'), id = product_id) 
        self.data['features'] = self.data['product'].feature.all()
        
        self.data['product_photos'] = ProductPhotos.objects.all().filter(product = self.data['product']) 

        for i in self.data['product_photos']:
            print(i)
        
        


        return render(request, self.template, self.data)


