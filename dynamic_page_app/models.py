from django.db import models
from django.core.validators import FileExtensionValidator

# Create your models here.
class ClientInfo(models.Model):
    title = models.CharField(max_length=100,unique=True)
    image = models.ImageField(upload_to = 'client_images',null =True,blank=True)

    class Meta:
        verbose_name = "Client"

    def __str__(self):
        return self.title



class CategoryItem(models.Model):
    title = models.CharField(max_length=100, blank=True, null=True)
    parent = models.ForeignKey(('self'), on_delete=models.DO_NOTHING, default='NULL', null=True, blank=True)
    icon = models.FileField(upload_to = 'category_icons',null =True,blank=True , validators=[FileExtensionValidator(['svg' , 'jpg','png' , 'jpeg'])])
    order = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        verbose_name = "Category"

    def __str__(self):
        return str(self.title)


class Feature(models.Model):
    title = models.CharField(max_length=100, blank=True, null=True)

    icon = models.FileField(upload_to = 'features',null =True,blank=True , validators=[FileExtensionValidator(['svg' , 'jpg','png' , 'jpeg'])])

    class Meta:
        verbose_name = "Future"

    def __str__(self):
        return str(self.title)

class ProductItem(models.Model):
    title = models.CharField(max_length=100, blank=True, null=True)
    code = models.PositiveIntegerField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    category = models.ForeignKey(CategoryItem, on_delete=models.DO_NOTHING, default='NULL', null=True, blank=True)
    order = models.PositiveIntegerField(blank=True, null=True)
    feature = models.ManyToManyField(Feature,  default='NULL', blank=True)


    photo_main = models.ImageField(upload_to = 'product_main_photos' , null = True )

    class Meta:
        verbose_name = "Product"

    def __str__(self):
        return str(self.title) + '---'  + str(self.category)


class ProductPhotos(models.Model):
    product  = models.ForeignKey(ProductItem , on_delete=models.CASCADE ,related_name='photos')
    image = models.ImageField(upload_to = 'product_photos')
