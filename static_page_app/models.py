from cgitb import enable
from django.db import models

# Create your models here.
class About_us(models.Model):
    title = models.CharField(max_length=100,unique=True, default='', null=True, blank=True)
    content = models.TextField(null=True,blank=True, default='')

    class Meta:
        verbose_name = "About Us Page"

    def __str__(self):
        return "Edit About Page"

class Contact_us(models.Model):
    page_title = models.CharField(max_length=100,unique=True, default='', null=True, blank=True)
    location_title = models.CharField(max_length=100,unique=True, default='', null=True, blank=True)
    address = models.CharField(max_length=100,unique=True, default='', blank=True)
    call_us_title = models.CharField(max_length=100,unique=True, default='', null=True, blank=True)
    phone_title = models.CharField(max_length=100,unique=True, default='', null=True, blank=True)
    fax_title = models.CharField(max_length=100,unique=True, default='', null=True, blank=True)
    phone1 = models.CharField(max_length=100,unique=True, default='', null=True, blank=True)
    phone2 = models.CharField(max_length=100,unique=True, default='', null=True, blank=True)
    fax = models.CharField(max_length=100,unique=True, default='', null=True, blank=True)
    connect_online_title = models.CharField(max_length=100,unique=True, default='', null=True, blank=True)
    email_title = models.CharField(max_length=100,unique=True, default='', null=True, blank=True)
    mail1 = models.CharField(max_length=100,unique=True, default='', null=True, blank=True)
    mail2 = models.CharField(max_length=100,unique=True, default='', null=True, blank=True)

    class Meta:
        verbose_name = "Contact Us Page"

    def __str__(self):
        return "Edit Contact Page"


class Request_Form(models.Model):
    fullname_placeholder = models.CharField(max_length=100,unique=True, default='', null=True, blank=True)
    phone_placeholder = models.CharField(max_length=100,unique=True, default='', blank=True)
    email_placeholder = models.CharField(max_length=100,unique=True, default='', null=True, blank=True)
    message_placeholder = models.CharField(max_length=100,unique=True, default='', null=True, blank=True)
    button_title = models.CharField(max_length=100,unique=True, default='', null=True, blank=True)

    class Meta:
        verbose_name = "Request Page"

    def __str__(self):
        return "Request Page"