from django.shortcuts import get_object_or_404, redirect, render
from solidcms__proj.view import BaseView
from static_page_app.models import About_us, Contact_us, Request_Form

# Create your views here.
class AboutUsClassView(BaseView):
    data = dict()
    template = "about.html"

    def get(self, request):
        self.data = dict()
        #Must have 
        self.static_translates()
        self.navbar()
        self.footer_contacts()
        self.footer_socials()
        
        #Multilang changer
        generate_url = str(request.get_full_path().split('/')[1])+ "/"
        self.data['page_url'] = str(request.get_full_path()).replace(str(generate_url), '')

        
        #Page specefic data
        about_us_page = get_object_or_404(About_us)
        self.data['page_title'] = about_us_page.title
        self.data['page_content'] = about_us_page.content
        self.data['page_meta_title'] = "Some text"
        return render(request, self.template, self.data)


class ContactUsClassView(BaseView):
    data = dict()
    template = "contact.html"

    def get(self, request):
        self.data = dict()
        #Must have 
        self.static_translates()
        self.navbar()
        self.footer_contacts()
        self.footer_socials()
        
        #Multilang changer
        generate_url = str(request.get_full_path().split('/')[1])+ "/"
        self.data['page_url'] = str(request.get_full_path()).replace(str(generate_url), '')

        #Page specefic data
        contact_us_page = get_object_or_404(Contact_us)
        self.data['page_title'] = contact_us_page.page_title
        self.data['location_title'] = contact_us_page.location_title
        self.data['call_us_title'] = contact_us_page.call_us_title
        self.data['phone_title'] = contact_us_page.phone_title
        self.data['fax_title'] = contact_us_page.fax_title
        self.data['fax'] = contact_us_page.fax
        self.data['phone1'] = contact_us_page.phone1
        self.data['address'] = contact_us_page.address
        self.data['connect_online_title'] = contact_us_page.connect_online_title
        self.data['email_title'] = contact_us_page.email_title
        self.data['mail1'] = contact_us_page.mail1
        self.data['mail2'] = contact_us_page.mail2
        #Request Form
        request_form = get_object_or_404(Request_Form)
        self.data['fullname_placeholder'] = request_form.fullname_placeholder
        self.data['phone_placeholder'] = request_form.phone_placeholder
        self.data['email_placeholder'] = request_form.email_placeholder
        self.data['message_placeholder'] = request_form.message_placeholder
        self.data['button_title'] = request_form.button_title


        return render(request, self.template, self.data)