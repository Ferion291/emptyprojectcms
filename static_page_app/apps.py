from django.apps import AppConfig


class StaticPageAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'static_page_app'
