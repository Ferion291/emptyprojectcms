from django.contrib import admin
from static_page_app.models import *

# Register your models here.
admin.site.register(About_us)
admin.site.register(Contact_us)
admin.site.register(Request_Form)