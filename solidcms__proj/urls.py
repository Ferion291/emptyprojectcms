"""solidcms__proj URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls import url
from dynamic_page_app.views import *

from static_page_app.views import *
from django.utils.translation import gettext_lazy as _

urlpatterns = i18n_patterns(
    path('admin/', admin.site.urls),    
    path('about/', AboutUsClassView.as_view(), name="AboutUsClassURL"),

    path('client/', ClientPageClassView.as_view(), name="ClientPageClassURL"),
    
    
    path('categories/', CategoryPageClassView.as_view(), name="CategoriesPageClassURL"),
    path('categories/products-list', CategoryPageByIDClassView.as_view(), name="CategoryPageClassURL"),

    path('categories/products/<int:product_id>/', ProductByIDClassView.as_view(), name="ProductIdPageClassURL"),
    
    
    path('contact/', ContactUsClassView.as_view(), name="ContactUsClassView"),
    path('i18n/', include('django.conf.urls.i18n')),
    path('', AboutUsClassView.as_view(), name="IndexURL"),
    

    path('cms/', ContactUsClassView.as_view(), name="ContactUsClassView"),
    
  prefix_default_language=True
) + static(prefix=settings.MEDIA_URL , document_root=settings.MEDIA_ROOT) 
   

#path('i18n/', include('django.conf.urls.i18n')),